package w_130130;

import java.util.ArrayList;

public class ObjectCasting {
	public static void main(String[] args){
		ArrayList<String> list1 = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		
		list1.add("정화영");
		list1.add("백진옥");
		list1.add("정미나");
		list1.add("이해림");
		
		System.out.println(list1);
		list2 = (ArrayList)list1.clone(); //object형을 리턴하므로 다운캐스팅해줘야함
		System.out.println(list2);
	}

}
