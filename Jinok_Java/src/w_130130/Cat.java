package w_130130;


public class Cat extends Animal{ //Animal객체 상속받음
	public Cat(String name){ 
		super(name); //부모 클래스의 구성자 사용
		
	}
	public void makeNoise(){ //오버라이딩
		System.out.println(name + " : 냐옹");
	}
}