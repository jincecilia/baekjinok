package w_130130;

public class GradeRecord implements Comparable<GradeRecord> { 
	//Comparable은 어떤 값을 비교할 수 있도록 compareTo()라는 메소드를 제공하는 인터페이스
	//Comparable을 구현하게 되면, compareTo() 메소드를 재정의하여, 어떤 값들의 비교 결과를 크다(1), 같다(0), 작다(-1)와 같은 정수형 값을 반환
	//더 큰 장점은 Comparable을 구현한 클래스는 배열로 사용될 때, Arrays.sort() 메소드를 이용하여, 자동 정렬 시킬 수 있음
	 
	protected String name;
	protected int grade;
	 
	public GradeRecord(){
		this.name = "무명";
		this.grade = 0;
		
	}
	public GradeRecord(String name, int grade){
		this.name = name;
		this.grade = grade;
		
	}
	
	public String getName(){
		return name;
	}
	public int getGrade(){
		return grade;
	}
	
	public String toString(){
		return "(이름 : " + name + "," + grade + ")";
	}

	public int compareTo(GradeRecord r2) {
		return this.grade - r2.grade;
	}


}
