package w_130130;

public class RecordExt extends Record implements Comparable<RecordExt>{

	int age;
	
	public RecordExt(String name, int grade, int age){
		super(name, grade); //부모 클래스인 Record의 구성자 사용
		this.age = age;
	}
	
	public String toString(){
		return  "(" + name + "," + grade +"," + age + ")";
	}
	
	public int bornIn(){
		return 2013-age;
	}
	
	public int getAge(){
		return age;
	}
	
	
	public int compareTo(RecordExt r2) {
		return this.age - r2.age;
	
	}

}
