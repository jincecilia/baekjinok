package w_130130;

public abstract class Record {
	protected String name;
	protected int grade;
	
	public Record(String name, int grade){
		this.name = name;
		this.grade = grade;
	}
	public String toString(){
		return name + " : " + grade;
	}
	
	public String getName(){
		return name;
	}
	public int getGrade(){
		return grade;
	}

}
